#include <ros/ros.h>
#include <collision_aware_joint_trajectory_wrapper/safe_command.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "safe_command");
  ros::NodeHandle nh;

  SafeCommand sc;
  ros::spin();

  return 0;
}
