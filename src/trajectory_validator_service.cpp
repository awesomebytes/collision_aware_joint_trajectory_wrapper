#include <ros/ros.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>
#include <moveit_msgs/Constraints.h>
#include <moveit_msgs/MoveItErrorCodes.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <trajectory_msgs/JointTrajectory.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <memory>


class TrajectoryValidatorService{
    //setup joint_trajectory_validator
private:
    const robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
    planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor_;
    collision_detection::CollisionRequest collision_request_;
    collision_detection::CollisionResult collision_result_;
    robot_trajectory::RobotTrajectoryPtr robot_traj_;
    const moveit_msgs::Constraints goal_constraints, path_constraints;

    //setup a method to the service to call
public:
    TrajectoryValidatorService()
        : robot_model_loader_(new robot_model_loader::RobotModelLoader("robot_description"))
        , planning_scene_monitor_(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader_))
    {

        robot_traj_.reset(new robot_trajectory::RobotTrajectory(robot_model_loader_->getModel(), ""));

        if(planning_scene_monitor_->getPlanningScene())
        {
            ROS_DEBUG_STREAM("Starting context monitors...");
            planning_scene_monitor_->startSceneMonitor();
            planning_scene_monitor_->startWorldGeometryMonitor();
            planning_scene_monitor_->startStateMonitor();
            ROS_DEBUG_STREAM("Context monitors started.");
        }
        else
        {
            ROS_ERROR_STREAM("Planning scene not configured!");
        }
    }

    //~TrajectoryValidatorService(); Bence told me to do this, i cry

    bool check_traj(moveit_msgs::ExecuteKnownTrajectory::Request  &req,
                    moveit_msgs::ExecuteKnownTrajectory::Response &res)
    {
        ROS_INFO_STREAM("RobotTrajectory received: " << req.trajectory);
        // construct RobotTrajectory from JointTrajectory message
        const moveit::core::RobotState& current_state = planning_scene_monitor_->getPlanningScene()->getCurrentState();
        robot_traj_->setRobotTrajectoryMsg(current_state, req.trajectory);
        ROS_INFO_STREAM("Set robot trajectory msg.");
        // validate trajectory using the planning scene
        if(planning_scene_monitor_->getPlanningScene()->isPathValid(*robot_traj_,
                                                                    path_constraints,
                                                                    goal_constraints,
                                                                    "",
                                                                    true)){
            res.error_code.val = moveit_msgs::MoveItErrorCodes::SUCCESS;
        ROS_INFO_STREAM("Success!.");
        }
        else{
            res.error_code.val =  moveit_msgs::MoveItErrorCodes::GOAL_IN_COLLISION;
            ROS_INFO_STREAM("Goal in collision :(");
        }
        ROS_INFO_STREAM("Returning... res.error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS ");
        return true;//(res.error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS);
    }

};



int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_validator_service");
    ros::NodeHandle n;

    TrajectoryValidatorService tvs;
    ros::ServiceServer service = n.advertiseService("check_trajectory_validity", &TrajectoryValidatorService::check_traj, &tvs);
    ROS_INFO("Ready to do check trajectories for collisions.");
    ros::spin();

    return 0;
}
