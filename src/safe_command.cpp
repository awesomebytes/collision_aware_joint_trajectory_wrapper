#include <collision_aware_joint_trajectory_wrapper/safe_command.h>
#include <moveit/collision_detection/collision_common.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_updater/diagnostic_updater.h>

SafeCommand::SafeCommand()
  : nh_("~")
  , robot_model_loader_(new robot_model_loader::RobotModelLoader("robot_description"))
  , planning_scene_monitor_(new planning_scene_monitor::PlanningSceneMonitor(robot_model_loader_))
  , diag_spinner_(1, &diag_queue_)
{
  if (!nh_.getParam("trajectory_check_time_step", traj_check_time_step_))
  {
    traj_check_time_step_ = 0.1;
  }
  ROS_INFO_STREAM("Checking given trajectories with a time step of: " << traj_check_time_step_ << "s.");
  robot_traj_.reset(new robot_trajectory::RobotTrajectory(robot_model_loader_->getModel(), ""));

  if(planning_scene_monitor_->getPlanningScene())
  {
    ROS_DEBUG_STREAM("Starting context monitors...");
    planning_scene_monitor_->startSceneMonitor("/move_group/monitored_planning_scene");
    planning_scene_monitor_->startWorldGeometryMonitor();
    planning_scene_monitor_->startStateMonitor();
    ROS_INFO_STREAM("Context monitors started for " << nh_.getNamespace());
  }
  else
  {
    ROS_ERROR_STREAM("Planning scene not configured for " << nh_.getNamespace());
  }

  cmd_pub_ = nh_.advertise<trajectory_msgs::JointTrajectory>("/cmd_pub", 1);
  ROS_INFO_STREAM("Publishing commands to: " << cmd_pub_.getTopic());

  cmd_sub_ = nh_.subscribe("/cmd_sub", 1, &SafeCommand::commandCb, this);
  ROS_INFO_STREAM("Listening to: " << cmd_sub_.getTopic());

  client_.reset(new FollowJointTrajectoryClient("/real_server", true));
  ROS_INFO_STREAM("Using action client on: " << nh_.resolveName("/real_server"));
  client_->waitForServer();

  server_.reset(new FollowJointTrajectoryServer(nh_,
                                                "follow_joint_trajectory",
                                                boost::bind(&SafeCommand::actionCb, this, _1),
                                                false));
  ROS_INFO_STREAM("Creating action server on: " << nh_.resolveName("follow_joint_trajectory"));
  server_->start();

  diag_nh_.setCallbackQueue(&diag_queue_);
  diag_spinner_.start();
  last_goal_result_ = true;
  last_reason_ = "No unsafe goals yet";
  diag_pub_ =  diag_nh_.advertise<diagnostic_msgs::DiagnosticArray>("/diagnostics", 1);
  diag_timer_ = diag_nh_.createTimer(ros::Duration(1),
                                     &SafeCommand::update_diagnostics,
                                     this);
}

void SafeCommand::commandCb(const trajectory_msgs::JointTrajectory &trajectory)
{
  ROS_INFO_STREAM("Command trajectory received: " << trajectory);

  std::string reason;
  if(checkTrajectoryEveryCheckTimeStep(trajectory, true, reason))
  {
    ROS_DEBUG_STREAM("Trajectory is safe, republishing");
    cmd_pub_.publish(trajectory);
    last_goal_result_ = true;
  }
  else
  {
    ROS_DEBUG_STREAM("Goal with problems...");
    last_reason_ = reason;
    last_goal_result_ = false;
    last_unsafe_goal_time_ = ros::Time::now();
  }
}

void SafeCommand::actionCb(const control_msgs::FollowJointTrajectoryGoalConstPtr &goal){
  ROS_DEBUG_STREAM("Goal received: " << goal->trajectory);

  std::string reason;
  if(checkTrajectoryEveryCheckTimeStep(goal->trajectory, true, reason))
  {
    ROS_DEBUG_STREAM("Trajectory is safe, republishing");
    // do action client send stuff with monitoring of stuffess
    client_->sendGoal(*goal,
                      boost::bind(&SafeCommand::doneCb, this, _1, _2),
                      FollowJointTrajectoryClient::SimpleActiveCallback(),
                      boost::bind(&SafeCommand::feedbackCb, this, _1));
    client_->waitForResult();
    last_goal_result_ = true;
  }
  else
  {
    ROS_DEBUG_STREAM("Trajectory found unsafe...");
    last_reason_ = reason;
    last_goal_result_ = false;
    last_unsafe_goal_time_ = ros::Time::now();

    // do actionserver aborted stuff
    control_msgs::FollowJointTrajectoryResult res;
    res.error_code = res.INVALID_GOAL;
    res.error_string = reason;
    server_->setAborted(res);
  }
}

void SafeCommand::doneCb(const actionlib::SimpleClientGoalState &,
                         const control_msgs::FollowJointTrajectoryResultConstPtr &result)
{
  if(result->error_code == result->SUCCESSFUL)
  {
    server_->setSucceeded(*result);
  }
  else
  {
    server_->setAborted(*result);
  }

}

void SafeCommand::feedbackCb(const control_msgs::FollowJointTrajectoryFeedbackConstPtr &feedback)
{
  server_->publishFeedback(*feedback);
}


bool SafeCommand::checkTrajectoryEveryCheckTimeStep(const trajectory_msgs::JointTrajectory &trajectory,
                                                    bool verbose, std::string &reason) const
{
  const moveit::core::RobotState& current_state = planning_scene_monitor_->getPlanningScene()->getCurrentState();
  // Trajectory must have minimum 2 points... being the first one where we are
  // this is because later on we will interpolate between points
  // to check safety and a minimum of two is needed
  // also, this way, we ensure that the trajectory to the first position
  // of the trajectory isn't dangerous (often not computed assuming we are
  // at the first point or very close to it)
  try
  {
    robot_traj_->setRobotTrajectoryMsg(current_state, trajectory);
    // Initial pose a tiny time after 0.0 (cannot be 0.0)
    robot_traj_->addPrefixWayPoint(current_state, traj_check_time_step_ / 2.0);
  }
  catch (moveit::Exception e)
  {
    ROS_ERROR_STREAM("Exception with given trajectory: " << e.what());
    std::stringstream ss;
    ss << "Exception with given trajectory: " << e.what();
    reason = ss.str();
    return false;
  }


  // Get final time
  const trajectory_msgs::JointTrajectoryPoint& last_point = trajectory.points.back();
  double final_time = last_point.time_from_start.toSec();

  bool result = true;

  // Loop for every timestep check the collision state
  // as we cannot trust that the trajectory points are too far
  // in between them hiding a collision
  ROS_DEBUG_STREAM("From the original trajectory of " << trajectory.points.size() <<
                   " points (+1 of initial pose) we check " <<
                   final_time / traj_check_time_step_ << " subpoints");
  int waypoints = robot_traj_->getWayPointCount();
  ros::WallTime init_t = ros::WallTime::now();
  for (double t_step=0.0; t_step <= final_time; t_step+=traj_check_time_step_)
  {
    robot_state::RobotStatePtr st(new robot_state::RobotState(current_state));
    ROS_DEBUG_STREAM("Going to get state at: " << t_step);

    // Interpolate without using getStateAtDurationFromStart as it seems bugged, we reproduce the behaviour here
    // getStateAtDurationFromStart checks if there are 0 points in traj, if so computes... and should be the inverse
    int before = 0, after = 0;
    // choose a blend (trajectory_time_part) relative of how far in the trajectory we are (in time of total)
    double trajectory_time_part = t_step / final_time;
    robot_traj_->findWayPointIndicesForDurationAfterStart(t_step, before, after, trajectory_time_part);
    double waypoints_part = after / (waypoints -1);
    // choose a blend time (t) [0.0 ... 1.0] in relation of our current waypoint of reference and total time
    double t = waypoints_part * trajectory_time_part;
    // waypoints_ is private... so I need to change this a bit
    robot_traj_->getWayPoint(before).interpolate(robot_traj_->getWayPoint(after), t, *st);
    //ROS_INFO_STREAM("St looks like: " << *st->getJointPositions("torso_lift_joint")); // very hardcoded debug

    collision_detection::CollisionRequest req;
    // To get some helpful ROS_INFO from underlying collision checking code
    req.verbose = verbose;
    // To check with all the robot
    req.group_name = "";
    // To store some information to show what collides
    req.max_contacts = 1;
    req.max_contacts_per_pair = 1;
    req.contacts = true;
    collision_detection::CollisionResult  res;

    planning_scene_monitor_->getPlanningScene()->checkCollision(req, res, *st);
    if (res.collision)
    {
      std::stringstream ss;
      // show all collisions, in practice one...
      ss << "Collision in between '";
      for(collision_detection::CollisionResult::ContactMap::const_iterator it = res.contacts.begin();
        it != res.contacts.end(); ++it)
      {
        ss << it->first.first << "' and '" << it->first.second << "'. (There could be more, only showing one).";
      }

      reason = ss.str();
      result = false;
    }
    // Only check feasibility if there was no collision, useless otherwise
    if (result)
    {
      if (! planning_scene_monitor_->getPlanningScene()->isStateFeasible(*st, verbose))
      {
        result = false;
        std::stringstream ss;
        ss << "State is not feasible on trajectory point at time " << t_step << " (although there was no collision detected).";
        reason = ss.str();
      }
    }
    // Stop checking the trajectory if a point is invalid
    if (!result)
    {
      break;
    }

  }
  ros::WallTime fin_t = ros::WallTime::now();
  ROS_INFO_STREAM("Safety check took: " << (fin_t - init_t).toSec() << "s.");
  return result;
}



void SafeCommand::update_diagnostics(const ros::TimerEvent &) const
{
  diagnostic_msgs::DiagnosticArray array;
  diagnostic_updater::DiagnosticStatusWrapper status;
  status.name = "Functionality: Safe command: " + cmd_pub_.getTopic();

  status.add("Last aborted execution reason", last_reason_);
  std::stringstream ss;
  ros::Duration time_ago = ros::Time::now() - last_unsafe_goal_time_;
  ss << "was time ago: " << std::setprecision(3) << time_ago.toSec() << "s.";
  status.add("Last aborted execution", ss.str());

  if (! last_goal_result_){
    status.mergeSummary(diagnostic_msgs::DiagnosticStatus::WARN,
                        "Last goal was NOT safe.");
  }
  else{
    status.mergeSummary(diagnostic_msgs::DiagnosticStatus::OK,
                        "Last goal was safe.");
  }

  array.status.push_back(status);

  diag_pub_.publish(array);
}
