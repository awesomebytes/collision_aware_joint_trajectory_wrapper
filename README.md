collision_aware_joint_trajectory_wrapper
===
An alternative `trajectory_msgs/JointTrajectory` command topic that checks for collisions before sending the goal to the real command topic. It uses MoveIt! scene monitor, so it will be aware of anything MoveIt! is aware.


For example, having `/arm_controller/command` `JointTrajectory` topic from `joint_trajectory_controller` you would wrap it up with `/arm_controller/safe_command`. 


The nodes that you don't trust its trajectories should send them to `/arm_controller/safe_command` which will only execute the trajectory if it has been found safe.

For example, to use `rqt_joint_trajectory_controller` in a safe way:

Launch the safe nodes for the robot:
````
<launch>
    <node pkg="collision_aware_joint_trajectory_wrapper" 
        name="safe_arm" type="safe_command" output="screen">
        <remap from="/cmd_pub" to="/arm_controller/command"/>
        <remap from="/cmd_sub" to="/arm_controller/safe_command"/>
    </node>
    <node pkg="collision_aware_joint_trajectory_wrapper" 
        name="safe_torso" type="safe_command" output="screen">
        <remap from="/cmd_pub" to="/torso_controller/command"/>
        <remap from="/cmd_sub" to="/torso_controller/safe_command"/>
    </node>
    <node pkg="collision_aware_joint_trajectory_wrapper" 
        name="safe_head" type="safe_command" output="screen">
        <remap from="/cmd_pub" to="/head_controller/command"/>
        <remap from="/cmd_sub" to="/head_controller/safe_command"/>
    </node>
</launch>
````


Launch `rqt_joint_trajectory_controller` using the wrapped topics with a launch file:
````
<launch>
    <node pkg="rqt_joint_trajectory_controller"
        name="rqt_joint_safe_tiago"
        type="rqt_joint_trajectory_controller" >
        <remap from="/arm_controller/command" to="/arm_controller/safe_command"/>
        <remap from="/torso_controller/command" to="/torso_controller/safe_command"/>
        <remap from="/head_controller/command" to="/head_controller/safe_command"/>
    </node>
</launch>
````

Or with a long rosrun:
````
rosrun rqt_joint_trajectory_controller rqt_joint_trajectory_controller /arm_controller/command:=/arm_controller/safe_command /torso_controller/command:=/torso_controller/safe_command /head_controller/command:=/head_controller/safe_command
````

