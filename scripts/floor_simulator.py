#!/usr/bin/env python
"""
Keep an eye that there is a floor for 
the robot to not plan into it.

Author: Sam Pfeiffer
"""
import rospy
from moveit_commander import PlanningSceneInterface
from moveit_msgs.srv import GetPlanningScene, GetPlanningSceneRequest, GetPlanningSceneResponse
from geometry_msgs.msg import Pose, PoseStamped


def wait_for_planning_scene_object(scene_srv, object_name='fake_floor'):
    rospy.loginfo("Waiting for object '" + object_name +
                  "'' to appear in planning scene...")
    gps_req = GetPlanningSceneRequest()
    # The names of the world objects
    # uint32 WORLD_OBJECT_NAMES=8
    # The geometry of the world objects
    # uint32 WORLD_OBJECT_GEOMETRY=16
    gps_req.components.components = gps_req.components.WORLD_OBJECT_NAMES
    # gps_req.components.WORLD_OBJECT_GEOMETRY could be added
    part_in_scene = False
    while not rospy.is_shutdown() and not part_in_scene:
        # This call takes a while when rgbd sensor is set
        gps_resp = scene_srv.call(gps_req)
        # check if 'part' is in the answer
        for collision_obj in gps_resp.scene.world.collision_objects:
            if collision_obj.id == object_name:
                part_in_scene = True
                break
        else:
            rospy.sleep(1.0)

    rospy.loginfo("'" + object_name + "'' is in scene!")


def check_if_floor_in_planning_scene(scene_srv, object_name='fake_floor'):
    rospy.loginfo("Checking for object '" + object_name +
                  "' in planning scene...")
    gps_req = GetPlanningSceneRequest()
    # The names of the world objects
    # uint32 WORLD_OBJECT_NAMES=8
    # The geometry of the world objects
    # uint32 WORLD_OBJECT_GEOMETRY=16
    gps_req.components.components = gps_req.components.WORLD_OBJECT_NAMES
    # gps_req.components.WORLD_OBJECT_GEOMETRY could be added
    # This call takes a while when rgbd sensor is set
    gps_resp = scene_srv.call(gps_req)
    # check if 'part' is in the answer
    for collision_obj in gps_resp.scene.world.collision_objects:
        if collision_obj.id == object_name:
            return True
    return False


if __name__ == '__main__':
    rospy.init_node('fake_floor')
    rospy.loginfo("Creating planning scene interface publisher")
    scene = PlanningSceneInterface()
    rospy.sleep(0.3)

    rospy.loginfo("Connecting to /get_planning_scene service")
    scene_srv = rospy.ServiceProxy('/get_planning_scene',
                                   GetPlanningScene)
    scene_srv.wait_for_service()

    p = PoseStamped()
    p.header.frame_id = 'base_footprint'
    p.pose.position.z = -0.01
    p.pose.orientation.w = 1.0
    scene.add_box("fake_floor", p, (1.5, 1.5, 0.01))

    wait_for_planning_scene_object(scene_srv)

    rospy.loginfo("Added fake_floor to world")

    r = rospy.Rate(2)
    while not rospy.is_shutdown():
        if not check_if_floor_in_planning_scene(scene_srv):
            rospy.loginfo("Re-adding fake_floor")
            scene.add_box("fake_floor", p, (1.0, 1.0, 0.01))
            rospy.sleep(1)
        r.sleep()
