#!/usr/bin/env python

import rospy
from copy import deepcopy
from sensor_msgs.msg import JointState
from moveit_msgs.srv import GetStateValidity, GetStateValidityRequest, GetStateValidityResponse
from moveit_msgs.msg import RobotState
from moveit_msgs.srv import ExecuteKnownTrajectory, ExecuteKnownTrajectoryRequest, ExecuteKnownTrajectoryResponse
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

# Build a useful mapping from MoveIt error codes to error names
from moveit_msgs.msg import MoveItErrorCodes
moveit_error_dict = {}
for name in MoveItErrorCodes.__dict__.keys():
    if not name[:1] == '_':
        code = MoveItErrorCodes.__dict__[name]
        moveit_error_dict[code] = name

def merge_js_with_jt(js, jt, jtp_id=0):
    """:js type: JointState
    :jt type: JointTrajectory
    :jtp_id type: int as the joint trajectory point to use the positions"""
    new_js = deepcopy(js)
    new_js.position = list(new_js.position)
    for j_idx, j_name in enumerate(jt.joint_names):
        idx = js.name.index(j_name)
        if idx == -1:
            rospy.logerr(
                "Joint " + j_name + " does not exist in joint_state " + str(js.name))
            # TODO: manage better
        new_js.position[idx] = jt.points[jtp_id].positions[j_idx]
    return new_js


class CollisionAwareJointTrajectoryCommand(object):
    def __init__(self):
        rospy.loginfo("Initializing CollisionAwareJointTrajectoryCommand...")
        self.last_js = None
        self.js_sub = rospy.Subscriber('/joint_states',
                                       JointState,
                                       self.js_cb,
                                       queue_size=5)
        rospy.loginfo("Subscribed to joint states at: " +
                      str(self.js_sub.resolved_name))
        self.cmd_pub = rospy.Publisher('command',
                                       JointTrajectory,
                                       queue_size=5)
        rospy.loginfo("Publishing commands to: " +
                      str(self.cmd_pub.resolved_name))
        # self.valid_srv = rospy.ServiceProxy('/check_state_validity',
        #                                     GetStateValidity,
        #                                     persistent=True)  # TODO: Ensure persistency if server reboots
        self.valid_srv = rospy.ServiceProxy('/check_trajectory_validity',
                                            ExecuteKnownTrajectory,
                                            persistent=True)
        rospy.loginfo("Checking validities with: " +
                      str(self.valid_srv.resolved_name))
        self.cmd_sub = rospy.Subscriber('safe_command',
                                        JointTrajectory,
                                        self.cmd_cb,
                                        queue_size=5)
        rospy.loginfo("Listening to commands at: " +
                      str(self.cmd_sub.resolved_name))

    def js_cb(self, js):
        self.last_js = js

    # def create_validity_request(self, jt):
    #     """:jt type: JointTrajectory"""
    #     rospy.loginfo("Creating validity request...")
    #     gsv_req = GetStateValidityRequest()
    #     # gsv_req.group_name  # TODO: Do we need this?
    #     gsv_req.group_name = 'arm_torso'
    #     gsv_req.robot_state.joint_state = merge_js_with_jt(
    #         self.last_js,
    #         jt,
    #         jtp_id=0)
    #     rospy.loginfo("Done.")
    #     return gsv_req

    def create_trajectory_validity(self, jt):
        req = ExecuteKnownTrajectoryRequest()
        req.trajectory.joint_trajectory = jt
        return req

    def check_validity(self, jt):
        req = self.create_trajectory_validity(jt)
        try:
            ini_t = rospy.Time.now()
            resp = self.valid_srv.call(req)
            fin_t = rospy.Time.now()
            rospy.loginfo("Response: " + str(resp) + " " + str(moveit_error_dict[resp.error_code.val]))

            rospy.loginfo("In time: " + str((fin_t - ini_t).to_sec()))
            if resp.error_code.val == 1:
                return True
            else:
                return False
        except rospy.ServiceException as e:
            rospy.logerr("Error when checking validity...")
            rospy.logerr("exception: " + str(e))
            # rospy.logerr("resp is: " + str(resp))
            # TODO: manage this
            return False

    def cmd_cb(self, jt):
        """:jt type: JointTrajectory"""
        rospy.loginfo("Got goal! Checking validity...")
        valid = self.check_validity(jt)
        if valid:
            self.cmd_pub.publish(jt)
        else:
            rospy.logerr("Collision found in trajectory, not executing")
            # TODO: more verbosity

if __name__ == '__main__':
    rospy.init_node('collision_aware_joint_trajectory_command')
    cajtc = CollisionAwareJointTrajectoryCommand()
    rospy.spin()
