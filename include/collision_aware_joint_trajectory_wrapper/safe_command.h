#ifndef SAFECOMMAND_H
#define SAFECOMMAND_H

#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>

#include <moveit/robot_trajectory/robot_trajectory.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <memory>

class SafeCommand
{
  //setup joint_trajectory_validator
public:
  SafeCommand();

  void commandCb(const trajectory_msgs::JointTrajectory &trajectory);

  void actionCb(const control_msgs::FollowJointTrajectoryGoalConstPtr &goal);

  void doneCb(const actionlib::SimpleClientGoalState &,
              const control_msgs::FollowJointTrajectoryResultConstPtr &result);

  void feedbackCb(const control_msgs::FollowJointTrajectoryFeedbackConstPtr &feedback);

  // Modification for our convenience of the original isPathValid
  // So we can show the collision links and ignore the path constraints
  bool isPathValid(const robot_trajectory::RobotTrajectory &trajectory,
                   bool verbose, std::string &reason) const;

  bool checkTrajectoryEveryCheckTimeStep(const trajectory_msgs::JointTrajectory &trajectory,
                                         bool verbose, std::string &reason) const;

  void update_diagnostics(const ros::TimerEvent &) const;

private:
  ros::NodeHandle nh_;
  const robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
  planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor_;

  // Action server wrapping
  typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> FollowJointTrajectoryClient;
  typedef actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> FollowJointTrajectoryServer;
  std::unique_ptr<FollowJointTrajectoryClient> client_;
  std::unique_ptr<FollowJointTrajectoryServer> server_;


  // Topic wrapping
  ros::Publisher cmd_pub_;
  ros::Subscriber cmd_sub_;

  // Time step for subtrajectory checking
  double traj_check_time_step_;

  robot_trajectory::RobotTrajectoryPtr robot_traj_;

  // Continuous test stuff
  ros::NodeHandle diag_nh_;
  ros::CallbackQueue diag_queue_;
  ros::AsyncSpinner diag_spinner_;
  ros::Publisher diag_pub_;
  ros::Timer diag_timer_;
  std::string last_reason_;
  ros::Time last_unsafe_goal_time_;
  bool last_goal_result_;
};

#endif // SAFECOMMAND_H
